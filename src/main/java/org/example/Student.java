package org.example;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Student {
    Long id;
    String name;
    String surname;
    String patronymic;
    Integer bornYear;
}
