package org.example;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<Student> {
    @Override
    public Student map(ResultSet rs, int i) throws SQLException {
        return Student.builder()
                .id(rs.getObject("id",Long.class))
                .name(rs.getObject("name",String.class))
                .surname(rs.getObject("surname",String.class))
                .patronymic(rs.getObject("patronymic",String.class))
                .bornYear(rs.getObject("born_year",Integer.class))
                .build();
    }
}
