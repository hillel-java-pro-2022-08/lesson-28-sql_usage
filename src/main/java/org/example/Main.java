package org.example;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        DataSource dataSource = new MyDataSource(
                "jdbc:postgresql://localhost:5432/bookstore",
                "example",
                "postgres"
        );
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        StudentRowMapper studentRowMapper = new StudentRowMapper();

//        List<Student> studentList = jdbcTemplate.query(
//                "SELECT id,name,surname,patronymic,born_year FROM students LIMIT 100",
//                studentRowMapper
//        );
//
//        System.out.println(studentList);
        String name = "vasia";
        Student student = jdbcTemplate.queryOne(
                "SELECT id,name,surname,patronymic,born_year FROM students WHERE name=?",
                new Object[]{name},
                studentRowMapper
        ).orElse(null);

        System.out.println(student);
    }
}
