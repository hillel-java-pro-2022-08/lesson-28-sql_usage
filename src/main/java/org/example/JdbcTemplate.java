package org.example;

import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class JdbcTemplate {
    private final DataSource dataSource;

    public <T> List<T> query(String sql, RowMapper<T> mapper) {
        return query(sql, new Object[0], mapper);
    }

    public <T> List<T> query(String sql, Object[] params, RowMapper<T> mapper) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = getStatement(sql, params, connection);
            ResultSet resultSet = stmt.executeQuery();
            List<T> list = new ArrayList<>(resultSet.getFetchSize());
            while (resultSet.next()) {
                T record = mapper.map(resultSet, resultSet.getRow());
                list.add(record);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int update(String sql, Object[] params) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = getStatement(sql, params, connection);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean execute(String sql, Object[] params) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = getStatement(sql, params, connection);
            return stmt.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private PreparedStatement getStatement(String sql, Object[] params, Connection connection) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(sql);
        for (int i = 0; i < params.length; i++) {
            stmt.setObject(i + 1, params[i]);
        }
        return stmt;
    }

    public <T> Optional<T> queryOne(String sql, Object[] params, RowMapper<T> mapper) {
        return query(sql, params, mapper).stream().findFirst();
    }
}
